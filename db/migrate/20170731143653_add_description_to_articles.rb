class AddDescriptionToArticles < ActiveRecord::Migration
  def change
    #Para criar um novo campo em uma tabela, através de uma nova migration:
    #add_column :nomedatabela, :nomedocampo, :tipo
    add_column :articles, :description, :text
    add_column :articles, :created_at, :datetime
    add_column :articles, :updated_at, :datetime
  end
end
