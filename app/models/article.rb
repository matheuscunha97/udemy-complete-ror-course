class Article < ActiveRecord::Base
  validates :title,  presence: true, length: {minimum: 3, maximum: 50} #presence:true = tornar um campo required
  validates :description,  presence: true,  length: {minimum: 10, maximum: 500}
end

  #No rails console:
  #Visualizar a tabela: Model.all
  #Inserir sem salvar: variavel = Model.new(campo: "valor", campo: "valor").
  #Salvando: variavel.save.
  #Inserindo e salvando de uma vez só: Model.create(campo: "", campo: "").
  #Para mostrar mensagens de erro no Database: model.errors.full_messages