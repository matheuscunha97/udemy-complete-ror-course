class ArticlesController < ApplicationController
  before_action :set_article, only: [:edit, :update, :show, :destroy]
  before_filter :redirect_cancel, only: [:create, :update]
  
  def index
    @articles = Article.all #@articles está no plural pois irá pegar TODOS os artigos que estão na base de dados
  end
  
  def new
    @article = Article.new #Instância do objeto Article
  end
  
  def edit
  end
  
  def show
  end
  
  def create
    @article = Article.new(article_params)
    if @article.save
      flash[:success] = "Article was sucessfully created."
      redirect_to article_path(@article)
    else
      render 'new' #Caso não seja criado, renderiza a action new.
    end
  end
  
  def update
    if @article.update(article_params)
      flash[:success] = "Article was sucessfully updated."
      redirect_to article_path(@article)
    else
      render 'edit' #Caso não seja editado, renderiza a action edit.
    end
  end
  
  def destroy
    @article.destroy
    flash[:danger] = "Article was sucessfully deleted."
    redirect_to articles_path
  end
  
  private
    def set_article
      @article = Article.find(params[:id])
    end
  
    def article_params
      params.require(:article).permit(:title, :description) #Parâmetros que serão passados para serem salvos
    end
    
    def redirect_cancel
      redirect_to root_path if params[:cancel]
    end
end